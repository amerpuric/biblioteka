﻿using System;
using System.Collections.Generic;
using Knjiga;
using System.Linq;
using Clan;
using System.Drawing;

namespace Biblioteka17243
{
    public sealed partial class Biblioteka
    {
        public List<Clan.Clan> listaClanova = new List<Clan.Clan>();
        public List<Knjiga.ObicnaKnjiga> listaKnjiga = new List<ObicnaKnjiga>();
        public List<Knjiga.ZaduzenaKnjiga> listaZaduzenihKnjiga = new List<Knjiga.ZaduzenaKnjiga>();
        public List<Clan.Uposlenik> listaUposlenika = new List<Clan.Uposlenik>();

        public void Admin()
        {

            RegistrujUposlenika("Amer", "Purić", Convert.ToDateTime("12/06/1995").Date, "1206995193044", "admin", "admin", null, 1, false);
        }

        public void RegistrujObicnuKnjigu(string n, string[] sa, string i, string z, string ni, int gi)
        {
            ObicnaKnjiga ok = new ObicnaKnjiga(n, sa, i, z, ni, gi);
            listaKnjiga.Add(ok);
        }

        public void RegistrujStrip(string n, string[] sa, string i, string z, string ni, int gi, string[] su, string iak, int bi, bool s)
        {
            Strip strip = new Strip(n, sa, i, z, ni, gi, su, iak, bi, s);
            listaKnjiga.Add(strip);
        }

        public void RegistrujNaucniRad(string n, string[] sa, string i, string z, string ni, int gi, string ik, string on)
        {
            NaucniRad nr = new NaucniRad(n, sa, i, z, ni, gi, ik, on);
            listaKnjiga.Add(nr);
        }

        public void IzbrisiKnjigu(string naslov)
        {
            foreach (Knjiga.ObicnaKnjiga k in listaKnjiga)
            {
                if (k.DajNaslov() == naslov)
                {
                    listaKnjiga.Remove(k);
                    return;
                }
            }
            throw new ArgumentException("Ne postoji knjiga sa tim naslovom.");
        }

        public void RegistrujClan(string i, string p, DateTime dr, string j, string k, int c, string ki, string l, Image s, bool u)
        {
            Clan.Clan oc = new Clan.Clan(i, p, dr, j, k, c, ki, l, s, u);
            listaClanova.Add(oc);
        }

        public void RegistrujBachelor(string i, string p, DateTime dr, string j, string k, int c, string ki, string l, Image s, int bi, bool u)
        {
            Clan.Bachelor b = new Clan.Bachelor(i, p, dr, j, k, c, ki, l, s, bi, u);
            listaClanova.Add(b);
        }

        public void RegistrujMaster(string i, string p, DateTime dr, string j, string k, int c, string ki, string l, Image s, int bi, bool u)
        {
            Clan.Master m = new Clan.Master(i, p, dr, j, k, c, ki, l, s, bi, u);
            listaClanova.Add(m);
        }

        public void RegistrujProfesor(string i, string p, DateTime dr, string j, string k, int c, string ki, string l, Image s, int sz, bool u)
        {
            Clan.Profesor pr = new Clan.Profesor(i, p, dr, j, k, c, ki, l, s, sz, u);
            listaClanova.Add(pr);
        }

        public void RegistrujUposlenika(string i, string p, DateTime dr, string j, string ki, string l, Image s, int st, bool ul)
        {
            Clan.Uposlenik u = new Clan.Uposlenik(i, p, dr, j, ki, l, s, st, ul);
            listaUposlenika.Add(u);
        }

        public void IzbrisiClana(int n)
        {
            foreach (Clan.Clan c in listaClanova)
            {
                if (c.DajSifru() == n)
                {
                    listaClanova.Remove(c);
                    return;
                }
            }
            throw new ArgumentException("Ne postoji član sa tom šifrom.");
        }

        public void IzbrisiUposlenika(int n)
        {
            foreach (var c in listaUposlenika)
            {
                if (c.SifraUposlenog == n)
                {
                    listaUposlenika.Remove(c);
                    return;
                }
            }
            throw new ArgumentException("Ne postoji uposlenik sa tom šifrom.");
        }

        public bool DaLiJeIzdata(string naslov)
        {
            foreach (Knjiga.ZaduzenaKnjiga zk in listaZaduzenihKnjiga)
            {
                if (zk.Knjiga.DajNaslov() == naslov)
                    return true;
            }
            return false;
        }

        public void ZaduziKnjigu(string naslov, int sifra)
        {
            foreach (Clan.Clan c in listaClanova)
            {
                if (c.DajSifru() == sifra)
                {
                    c.PovecajZaduzenja();
                }
            }

            foreach (Knjiga.ObicnaKnjiga k in listaKnjiga)
            {
                if (k.DajNaslov() == naslov)
                {
                    DateTime datumVracanja = DateTime.Now.AddDays(25);
                    ZaduzenaKnjiga zk = new ZaduzenaKnjiga(k, datumVracanja);
                    listaZaduzenihKnjiga.Add(zk);
                    k.zaduzena = true;
                }
            }
        }

        public void VratiKnjigu(string naslov, int sifra)
        {
            if (!DaLiJeIzdata(naslov))
            {
                throw new Exception("Knjiga sa tim naslovom nije zaduzena.");
            }

            foreach (Knjiga.ZaduzenaKnjiga zk in listaZaduzenihKnjiga.ToList())
            {
                if (zk.Knjiga.DajNaslov() == naslov)
                {
                    listaZaduzenihKnjiga.Remove(zk);
                    zk.Knjiga.zaduzena = false;
                }
            }
            foreach (Clan.Clan c in listaClanova)
            {
                if (c.DajSifru() == sifra)
                {
                    c.SmanjiZaduzenja();
                }
            }
        }

        public string NadjiKnjigu(int sifra)
        {
            int br = 0;
            string knjiga = "";
            foreach (Knjiga.Knjiga k in listaKnjiga)
            {
                if (k.DajSifru() == sifra)
                {
                    knjiga = k.DajNaslov() + " " + k.DajZanr() + " " + k.DajSifru();
                    br++;
                }
            }
            if (br == 0)
            {
                knjiga = "Ne postoji knjiga sa tom sifrom.";
            }

            return knjiga;
        }

        public string NadjiKnjiguPoNaslov(string naslov)
        {
            int br = 0;
            string knjiga = "";
            foreach (Knjiga.Knjiga k in listaKnjiga)
            {
                if (k.DajNaslov() == naslov)
                {
                    knjiga = k.DajNaslov() + " " + k.DajZanr() + " " + k.DajSifru();
                    br++;
                }
            }
            if (br == 0)
            {
                knjiga = "Ne postoji knjiga sa tim naslovom.";
            }

            return knjiga;
        }


        public string NadjiClana(int sifra)
        {
            int br = 0;
            string clan = "";
            foreach (Clan.Clan c in listaClanova)
            {
                if (c.DajSifru() == sifra)
                {
                    clan = c.DajIme() + " " + c.DajPrezime() + " " + c.DajSifru();
                    br++;
                }
            }
            if (br == 0)
            {
                clan = "Ne postoji clan sa tom sifrom.";
            }

            return clan;
        }

        public string NadjiClanaPoImenu(string ime) 
        {
            int br = 0;
            string clan = "";
            foreach (Clan.Clan c in listaClanova)
            {
                if (c.DajIme() == ime)
                {
                    clan = c.DajIme() + " " + c.DajPrezime() + " " + c.DajSifru();
                    br++;
                }
            }
            if (br == 0)
            {
                clan = "Ne postoji clan sa tom sifrom.";
            }

            return clan;
        }
    }
}

