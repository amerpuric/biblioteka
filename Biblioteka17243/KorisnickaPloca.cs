﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka17243
{
    public partial class KorisnickaPloca : Form
    {
        public Biblioteka b1;
        public int kojiJeClan = 0;
        public int kojaJeKnjiga = 0;
        public int zr = 0;
        public int brisi = 0;
        public KorisnickaPloca(Biblioteka b)
        {
            b1 = b;
            InitializeComponent();
            članoviToolStripMenuItem.Visible = false;
            uposleniciToolStripMenuItem.Visible = false;
            knjigeToolStripMenuItem.Visible = false;
            label13.Visible = false;
            label14.Visible = false;

            foreach (var k in b1.listaUposlenika)
            {
                if(k.Ulogovan == true)
                {
                    if(k.Status == 1)
                    {
                        uposleniciToolStripMenuItem.Visible = true;
                    }
                    članoviToolStripMenuItem.Visible = true;
                    knjigeToolStripMenuItem.Visible = true;
                    label4.Text = k.Ime;
                    label5.Text = k.Prezime;
                    label6.Text = k.DatumRodjenja.ToShortDateString();
                    label8.Text = k.Jmbg;
                    if(k.Status == 1)
                    {
                        label10.Text = "Admin";
                    }
                    else if(k.Status == 2)
                    {
                        label10.Text = "Uposlenik";
                    }
                    label12.Text = k.SifraUposlenog.ToString();
                    pictureBox1.Image = k.Slika;
                }

            }

            foreach (var k in b1.listaClanova)
            {
                
                if (k.Ulogovan == true)
                {
                    label4.Text = k.DajIme();
                    label5.Text = k.DajPrezime();
                    label6.Text = k.DajDatumRodjenja().ToShortDateString();
                    label8.Text = k.DajJMBG();
                    label10.Text = "Član";
                    label12.Text = k.DajSifru().ToString();
                    pictureBox1.Image = k.Slika;
                    label14.Text = k.DajBrojZaduzenihKnjiga().ToString();
                    label13.Visible = true;
                    label14.Visible = true;

                }
            }
        }

        private void dodajObičnogČlanaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kojiJeClan = 1;
            UnosClana unos = new UnosClana(b1, kojiJeClan);
            unos.FormClosed += new FormClosedEventHandler(unos_FormClosed);
            unos.label1.Visible = false;
            unos.textBox6.Visible = false;
            this.Hide();
            unos.Show();
        }

        void unos_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        void pregled_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        void zaduziRazduzi_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Da li ste sigurni?", "Biblioteka17243", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes)
            {
                foreach (var k in b1.listaUposlenika)
                {
                    if(k.Ulogovan == true)
                    {
                        k.Ulogovan = false;
                    }
                }

                foreach(var k in b1.listaClanova)
                {
                    if (k.Ulogovan == true)
                    {
                        k.Ulogovan = false;
                    }
                }
                Close();
            }  
        }

        private void bachelorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kojiJeClan = 2;
            UnosClana unos = new UnosClana(b1, kojiJeClan);
            unos.FormClosed += new FormClosedEventHandler(unos_FormClosed);
            unos.label1.Text = "Index:";
            this.Hide();
            unos.Show();
        }

        private void masterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kojiJeClan = 3;
            UnosClana unos = new UnosClana(b1, kojiJeClan);
            unos.FormClosed += new FormClosedEventHandler(unos_FormClosed);
            unos.label1.Text = "Index:";
            this.Hide();
            unos.Show();
        }

        private void profesorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kojiJeClan = 4;
            UnosClana unos = new UnosClana(b1, kojiJeClan);
            unos.FormClosed += new FormClosedEventHandler(unos_FormClosed);
            unos.label1.Text = "Šifra zaposlenog:";
            this.Hide();
            unos.Show();
        }

        private void običnaKnjigaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kojaJeKnjiga = 1;
            UnosKnjige unos = new UnosKnjige(b1, kojaJeKnjiga);
            unos.FormClosed += new FormClosedEventHandler(unos_FormClosed);
            unos.label7.Visible = false;
            unos.label8.Visible = false;
            unos.label9.Visible = false;
            unos.label10.Visible = false;
            unos.label11.Visible = false;
            unos.label12.Visible = false;
            unos.textBox7.Visible = false;
            unos.textBox8.Visible = false;
            unos.textBox9.Visible = false;
            unos.radioButton1.Visible = false;
            unos.radioButton2.Visible = false;
            unos.label13.Visible = false;
            unos.textBox3.Visible = false;
            this.Hide();
            unos.Show();
        }

        private void stripToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kojaJeKnjiga = 2;
            UnosKnjige unos = new UnosKnjige(b1, kojaJeKnjiga);
            unos.FormClosed += new FormClosedEventHandler(unos_FormClosed);
            
            unos.label11.Visible = false;
            unos.label10.Visible = false;
            this.Hide();
            unos.Show();
        }

        private void naučniRadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kojaJeKnjiga = 3;
            UnosKnjige unos = new UnosKnjige(b1, kojaJeKnjiga);
            unos.FormClosed += new FormClosedEventHandler(unos_FormClosed);
            unos.label7.Visible = false;
            unos.label8.Visible = false;
            unos.label9.Visible = false;
            unos.label12.Visible = false;
            unos.textBox9.Visible = false;
            unos.radioButton1.Visible = false;
            unos.radioButton2.Visible = false;
            unos.label13.Visible = false;
            unos.textBox3.Visible = false;
            this.Hide();
            unos.Show();
        }

        private void dodajUposlenikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnosUposlenika unos = new UnosUposlenika(b1);
            unos.FormClosed += new FormClosedEventHandler(unos_FormClosed);
            this.Hide();
            unos.Show();
        }

        private void pregledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pregled pregled = new Pregled(b1);
            pregled.FormClosed += new FormClosedEventHandler(pregled_FormClosed);
            this.Hide();
            pregled.Show();
        }

        private void zadužiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            zr = 1;
            ZadužiRazduži nova = new ZadužiRazduži(b1, zr);
            nova.FormClosed += new FormClosedEventHandler(zaduziRazduzi_FormClosed);
            this.Hide();
            nova.Text = "Zaduži";
            nova.Show();
        }

        private void razdužiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            zr = 2;
            ZadužiRazduži nova = new ZadužiRazduži(b1, zr);
            nova.FormClosed += new FormClosedEventHandler(zaduziRazduzi_FormClosed);
            this.Hide();
            nova.Text = "Razduži";
            nova.Show();
        }

        private void izbrišiČlanaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            brisi = 1;
            ObrisiClanaKnjiguUposlenika nova = new ObrisiClanaKnjiguUposlenika(b1, brisi);
            nova.FormClosed += new FormClosedEventHandler(obrisi_FormClosed);
            this.Hide();
            nova.Text = "Briši člana";
            nova.label1.Text = "Šifra člana:";
            nova.Show();
        }

        void obrisi_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void izbrišiKnjiguToolStripMenuItem_Click(object sender, EventArgs e)
        {
            brisi = 2;
            ObrisiClanaKnjiguUposlenika nova = new ObrisiClanaKnjiguUposlenika(b1, brisi);
            nova.FormClosed += new FormClosedEventHandler(obrisi_FormClosed);
            this.Hide();
            nova.Text = "Briši knjigu";
            nova.label1.Text = "Naslov knjige:";
            nova.Show();
        }

        private void izbrišiUposlenikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            brisi = 3;
            ObrisiClanaKnjiguUposlenika nova = new ObrisiClanaKnjiguUposlenika(b1, brisi);
            nova.FormClosed += new FormClosedEventHandler(obrisi_FormClosed);
            this.Hide();
            nova.Text = "Briši uposlenika";
            nova.label1.Text = "Šifra uposlenog:";
            nova.Show();
        }
    }
}
