﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka17243
{
    public partial class ObrisiClanaKnjiguUposlenika : Form
    {
        public Biblioteka b1;
        public int staDaRadim;
        public ObrisiClanaKnjiguUposlenika(Biblioteka b, int sdr)
        {
            b1 = b;
            staDaRadim = sdr;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool proslo = true;

            if (staDaRadim == 1)
            {
                try
                {
                    b1.IzbrisiClana(Convert.ToInt32(textBox1.Text));
                }
                catch (Exception greska)
                {
                    proslo = false;
                    MessageBox.Show(greska.Message, "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (proslo)
                {
                    MessageBox.Show("Član uspješno izbrisan.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
            }
            else if (staDaRadim == 2)
            {
                try
                {
                    b1.IzbrisiKnjigu(textBox1.Text);
                }
                catch (Exception greska)
                {
                    proslo = false;
                    MessageBox.Show(greska.Message, "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                if (proslo)
                {
                    MessageBox.Show("Knjiga uspješno obrisana.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
            }
            else if(staDaRadim == 3)
            {
                try
                {
                    b1.IzbrisiUposlenika(Convert.ToInt32(textBox1.Text));
                }
                catch (Exception greska)
                {
                    proslo = false;
                    MessageBox.Show(greska.Message, "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (proslo)
                {
                    MessageBox.Show("Uposlenik uspješno izbrisan.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
            }
              

            }
        }
    }

