﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Biblioteka17243;
using Knjiga;

namespace Biblioteka17243
{
    public partial class Prijava : Form
    {
        public static Biblioteka biblioteka = new Biblioteka();
           
        public Prijava()
        {
            InitializeComponent();
            biblioteka.Admin();
            biblioteka.RegistrujUposlenika("Jana", "Voda", Convert.ToDateTime("15/02/1947"), "15029474444", "jana", "jana", null, 2, false);
            biblioteka.RegistrujBachelor("Rudolf", "Maus", Convert.ToDateTime("01/08/1998"), "0108998172222",  "", 1, "maus" , "maus" , null, 17589, false);
            biblioteka.RegistrujClan("Walt", "Pepsi", Convert.ToDateTime("11/06/1993"), "1106993178965", "", 2, "walt", "walt", null, false);
            biblioteka.RegistrujMaster("Tom", "Cat", Convert.ToDateTime("18/12/1995"), "1812995178265", "", 1, "tom", "tom", null, 17555, false);
            biblioteka.RegistrujProfesor("Jerry", "Mouse", Convert.ToDateTime("28/10/1999"), "2810999172365", "", 1, "jerry", "jerry", null, 35, false);
            string G = "Ivo Andrić,IA";
            string[] G1 = G.Split(',');
            biblioteka.RegistrujObicnuKnjigu("Prokleta avlija", G1, "ISBN 1-23654-984-6", "Magični realizam", "XY", 1981);
            string S = "A.K.,M.M.";
            string[] S1 = S.Split(',');
            string C = "Y.R.,T.N.,L.T.";
            string[] C1 = C.Split(',');
            bool spe = true;
            biblioteka.RegistrujStrip("Batman", S1, "ISBN 1-36958-445-9", "Akcija", "DC", 2002, C1, "Aero", 5, spe);
            string A = "Ajna T., Dean Winchester , Sam Winchester";
            string[] A1 = A.Split(',');
            biblioteka.RegistrujNaucniRad("Show line vs Working line", A1, "ISBN 9 65456 654 8", "Naucni", "Future Hope", 2015, "CRUFTS", "Animals");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool postoji = false;

            foreach (var k in biblioteka.listaUposlenika)
            {
                if (k.KorisnickoIme == textBox1.Text && k.Lozinka == textBox2.Text)
                {
                    postoji = true;
                    k.Ulogovan = true;
                }
            }

            foreach (var k in biblioteka.listaClanova)
            {
                if (k.KorisnickoIme == textBox1.Text && k.Lozinka == textBox2.Text)
                {
                    postoji = true;
                    k.Ulogovan = true;
                }
            }

            if (postoji)
            {
                var result = MessageBox.Show("Uspješna prijava.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (result == DialogResult.OK)
                {
                    KorisnickaPloca kp = new KorisnickaPloca(biblioteka);
                    kp.FormClosed += new FormClosedEventHandler(kp_FormClosed);
                    this.Hide();
                    kp.Show();
                }
            }
            else
            {
                var result = MessageBox.Show("Pogrešni podaci.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if(result == DialogResult.OK)
                {
                    textBox1.Clear();
                    textBox2.Clear();
                    textBox1.Focus();
                }
            }
        }

        void kp_FormClosed(object sender, FormClosedEventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            this.Show();
        }
    }
}
