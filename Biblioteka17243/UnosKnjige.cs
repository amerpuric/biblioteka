﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka17243
{
    public partial class UnosKnjige : Form
    {
        public Biblioteka b1;
        public int k;
        public UnosKnjige(Biblioteka b, int knjiga)
        {
            b1 = b;
            k = knjiga;
            InitializeComponent();
        }

        public void pocistiInformacijeOUnosu()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";

            radioButton1.Checked = false;
            radioButton2.Checked = false;

            maskedTextBox1.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Da li ste sigurni?", "Feedback", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Close();
            }
        }

        private void UnosKnjige_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool proslo = true;
            bool specijal = false;

            if(radioButton2.Checked)
            {
                specijal = true;
            }
            
            try
            {
                string[] spisakAutora = textBox2.Text.Split(',');
                if (k == 1)
                {
                    b1.RegistrujObicnuKnjigu(textBox1.Text, spisakAutora, "ISBN " + maskedTextBox1.Text, textBox5.Text, textBox6.Text, Convert.ToInt32(textBox4.Text));
                }
                else if (k == 2)
                {
                    string[] spisakUmjetnika = textBox3.Text.Split(',');
                    b1.RegistrujStrip(textBox1.Text, spisakAutora, "ISBN " + maskedTextBox1.Text, textBox5.Text, textBox6.Text, Convert.ToInt32(textBox4.Text), spisakUmjetnika, textBox8.Text, Convert.ToInt32(textBox9.Text), specijal);
                }
                else if (k == 3)
                {
                    b1.RegistrujNaucniRad(textBox1.Text, spisakAutora, "ISBN " + maskedTextBox1.Text, textBox5.Text, textBox6.Text, Convert.ToInt32(textBox4.Text), textBox7.Text, textBox8.Text);
                }
            }
            catch (Exception greska)
            {
                proslo = false;
            }
            if (proslo)
            {
                MessageBox.Show("Knjiga uspješno registrovana.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                MessageBox.Show("Pogrešni podaci.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Error);
                pocistiInformacijeOUnosu();
            }
        }
    }
}
