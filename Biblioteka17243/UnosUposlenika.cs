﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka17243
{
    public partial class UnosUposlenika : Form
    {
        public Biblioteka b1;
        public UnosUposlenika(Biblioteka b)
        {
            b1 = b;
            InitializeComponent();
        }

        public void pocistiInformacijeOUnosu()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            kontrolaSlika1.pictureBox1.Image = null;
            kontrolaSlika1.dateTimePicker1.Value = DateTime.Now;
            dateTimePicker1.Value = DateTime.Now;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Da li ste sigurni?", "Feedback", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            bool proslo = true;
            
            try
            {
                b1.RegistrujUposlenika(textBox1.Text, textBox2.Text, dateTimePicker1.Value, textBox3.Text, textBox4.Text, textBox5.Text, kontrolaSlika1.dajSliku(), 2, false);
            }
            catch (Exception greska)
            {
                proslo = false;
            }
            if (proslo)
            {
                MessageBox.Show("Uposlenik uspješno registrovan.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                MessageBox.Show("Pogrešni podaci.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Error);
                pocistiInformacijeOUnosu();
            }
        }
    }
}
