﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka17243
{
    public partial class Pregled : Form
    {
        public Biblioteka b1;
        public Pregled(Biblioteka b)
        {
            b1 = b;
            InitializeComponent();

            button3.Visible = false;
            listBox3.Visible = false;
            

            foreach(var k in b1.listaUposlenika)
            {
                if(k.Ulogovan == true)
                {
                    if(k.Status == 1)
                    {
                        button3.Visible = true;
                        listBox3.Visible = true;
                    }
                }
            }

            foreach(var k in b1.listaClanova)
            {
                if(k.Ulogovan == true)
                {
                    listBox2.Visible = false;   
                    button2.Visible = false;
                    listBox3.Visible = false;
                    button3.Visible = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox2.Items.Clear();

            foreach (var k in b1.listaClanova)
            {
                listBox2.Items.Add(k.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            foreach(var k in b1.listaKnjiga)
            {
                listBox1.Items.Add(k.ToString());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox3.Items.Clear();

            foreach(var k in b1.listaUposlenika)
            {
                listBox3.Items.Add(k.ToString());
            }
        }
    }
}
