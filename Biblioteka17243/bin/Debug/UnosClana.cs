﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clan;
using Biblioteka17243;


namespace Biblioteka17243
{
    public partial class UnosClana : Form
    {
        public Biblioteka b1;
        public int c;
        public UnosClana(Biblioteka b, int clan)
        {
            c = clan;
            b1 = b;
            InitializeComponent();
    }
        public void pocistiInformacijeOUnosu()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            
            kontrolaSlika1.pictureBox1.Image = null;
            kontrolaSlika1.dateTimePicker1.Value = DateTime.Now;
            dateTimePicker1.Value = DateTime.Now;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Da li ste sigurni?", "Feedback", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes)
            {
                Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int clanstvo = 0;
            bool proslo = true;
            if(radioButton1.Checked)
            {
                clanstvo = 1;
            }
            try
            {
                if (c == 1)
                {
                    b1.RegistrujClan(textBox1.Text, textBox2.Text, dateTimePicker1.Value, textBox3.Text, "", clanstvo, textBox4.Text, textBox5.Text, kontrolaSlika1.dajSliku(), false);
                }
                else if(c == 2)
                {
                    b1.RegistrujBachelor(textBox1.Text, textBox2.Text, dateTimePicker1.Value, textBox3.Text, "", clanstvo, textBox4.Text, textBox5.Text, kontrolaSlika1.dajSliku(), Convert.ToInt32(textBox6.Text), false);
                }
                else if(c == 3)
                {
                    b1.RegistrujMaster(textBox1.Text, textBox2.Text, dateTimePicker1.Value, textBox3.Text, "", clanstvo, textBox4.Text, textBox5.Text, kontrolaSlika1.dajSliku(), Convert.ToInt32(textBox6.Text), false);
                }
                else if(c == 4)
                {
                    b1.RegistrujProfesor(textBox1.Text, textBox2.Text, dateTimePicker1.Value, textBox3.Text, "", clanstvo, textBox4.Text, textBox5.Text, kontrolaSlika1.dajSliku(), Convert.ToInt32(textBox6.Text), false);
                }
            }
            catch(Exception greska)
            {
                proslo = false;
            }
            if (proslo)
            {
                MessageBox.Show("Član uspješno registrovan.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                MessageBox.Show("Pogrešni podaci.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Error);
                pocistiInformacijeOUnosu();
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
