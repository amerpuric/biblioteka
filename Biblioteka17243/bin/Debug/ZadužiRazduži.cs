﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka17243
{
    public partial class ZadužiRazduži : Form
    {
        public Biblioteka b1;
        public int s;
        public ZadužiRazduži(Biblioteka b, int sta)
        {
            b1 = b;
            s = sta;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool proslo = false;
            if(s == 1)
            {
                b1.ZaduziKnjigu(textBox1.Text, Convert.ToInt32(textBox2.Text));
                MessageBox.Show("Knjiga je uspješno zadužena.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else if(s == 2)
            {
                try
                {
                    b1.VratiKnjigu(textBox1.Text, Convert.ToInt32(textBox2.Text));
                }
                catch (Exception greska)
                {
                    proslo = false;
                    MessageBox.Show(greska.Message, "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (proslo)
                {
                    MessageBox.Show("Knjiga je uspješno razdužena.", "Biblioteka17243", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
            }
        }
    }
}
