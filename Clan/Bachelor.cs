﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Clan
{
    public class Bachelor : Clan, Clanovi
    {
        public int brojIndexa;
        public double popust = 0.1;

        public Bachelor(string i, string p, DateTime dr, string j, string k, int c, string ki, string l, Image s, int bi, bool u) : base(i, p, dr, j, k, c , ki, l, s, u)
        {
            brojIndexa = bi;
        }

        public override string DajIme()
        {
            return ime;
        }

        public override string DajPrezime()
        {
            return prezime;
        }

        public override DateTime DajDatumRodjenja()
        {
            return datumRodjenja;
        }

        public override string DajJMBG()
        {
            return jmbg;
        }

        public override int DajSifru()
        {
            return sifra;
        }

        public override string DajKomentar()
        {
            return komentar;
        }

        public int DajBrojIndexa()
        {
            return brojIndexa;
        }

        public override int DajBrojZaduzenihKnjiga()
        {
            return brojZaduzenihKnjiga;
        }

        public override void PovecajZaduzenja()
        {
            brojZaduzenihKnjiga++;
        }

        public override void SmanjiZaduzenja()
        {
            brojZaduzenihKnjiga--;
        }

        public override string Clanstvo(int n)
        {
            if (n == 1)
            {
                return "Mjesecno";
            }
            else
            {
                return "Godisnje";
            }

        }

        public override double DajCijenu()
        {
            if (clanstvo == 1)
            {
                return cijena * (1 - popust);
            }
            else
                return cijena * (1 - popust) * 10;
        }

        public override string ToString()
        {
            return ime + " " + prezime + " " + sifra + " " + "(bachelor)" + "\n";
        }


    }
}
