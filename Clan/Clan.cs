﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Drawing;

namespace Clan
{
    public class Clan : Clanovi
    {
        static int broj = 1;
        public string ime;
        public string prezime;
        public string jmbg;
        public DateTime datumRodjenja;
        public int sifra;
        public string komentar;
        //private const string JMBG = "^[0 - 9]{13}$";
        public int clanstvo;
        public int brojZaduzenihKnjiga = 0;
        public static double cijena = 20;
        private string korisnickoIme;
        private string lozinka;
        private Image slika;
        bool ulogovan;

        public string KorisnickoIme
        {
            get
            {
                return korisnickoIme;
            }

            set
            {
                korisnickoIme = value;
            }
        }

        public string Lozinka
        {
            get
            {
                return lozinka;
            }

            set
            {
                lozinka = value;
            }
        }

        public Image Slika
        {
            get
            {
                return slika;
            }

            set
            {
                slika = value;
            }
        }

        public bool Ulogovan
        {
            get
            {
                return ulogovan;
            }

            set
            {
                ulogovan = value;
            }
        }

        public Clan(string i, string p, DateTime dr, string j, string k, int c, string ki, string l, Image s, bool u)
        {
            //Regex regex = new Regex(JMBG);
            //if(!regex.IsMatch(j))
            //{
            //    throw new ArgumentException("Pogresan JMBG.");
            //}

            string danRodjenja = dr.Day.ToString();
            if (dr.Day < 10)
                danRodjenja = "0" + danRodjenja;

            string mjesecRodjenja = dr.Month.ToString();
            if (dr.Month < 10)
                mjesecRodjenja = "0" + mjesecRodjenja;

            string godinaRodjenja = dr.Year.ToString();

            if (!(j[0] == danRodjenja[0] && j[1] == danRodjenja[1] && j[2] == mjesecRodjenja[0] && j[3] == mjesecRodjenja[1] && j[4] == godinaRodjenja[1] && j[5] == godinaRodjenja[2] && j[6] == godinaRodjenja[3]))
                throw new ArgumentException("Pogresan JMBG.");

            ime = i;
            prezime = p;
            datumRodjenja = dr;
            jmbg = j;
            komentar = k;
            clanstvo = c;
            korisnickoIme = ki;
            lozinka = l;
            slika = s;
            ulogovan = u;
            sifra = broj++;
        }

        public virtual string DajIme()
        {
            return ime;
        }

        public virtual string DajPrezime()
        {
            return prezime;
        }

        public virtual DateTime DajDatumRodjenja()
        {
            return datumRodjenja;
        }

        public virtual string DajJMBG()
        {
            return jmbg;
        }

        public virtual int DajSifru()
        {
            return sifra;
        }

        public virtual string DajKomentar()
        {
            return komentar;
        }

        public virtual int DajBrojZaduzenihKnjiga()
        {
            return brojZaduzenihKnjiga;
        }

        public virtual void PovecajZaduzenja()
        {
            brojZaduzenihKnjiga++;
        }

        public virtual void SmanjiZaduzenja()
        {
            brojZaduzenihKnjiga--;
        }

        public virtual string Clanstvo(int n)
        {
            if (n == 1)
            {
                return "Mjesecno";
            }
            else
            {
                return "Godisnje";
            }
                
        }

        public virtual double DajCijenu()
        {
            if (clanstvo == 1)
            {
                return cijena;
            }
            else
                return cijena * 10;
        }

        public override string ToString()
        {
            return ime + " " + prezime + " " + sifra + "\n";
        }
    }
}
