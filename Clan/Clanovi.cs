﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clan
{
    interface Clanovi
    {
        string DajIme();
        string DajPrezime();
        string DajJMBG();
        DateTime DajDatumRodjenja();
        int DajSifru();
        string DajKomentar();
        int DajBrojZaduzenihKnjiga();
        void PovecajZaduzenja();
        void SmanjiZaduzenja();
        string Clanstvo(int i);
        double DajCijenu();
    }
}
