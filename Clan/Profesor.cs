﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Clan
{
    public class Profesor : Clan, Clanovi
    {
        public double popust = 0.15;
        public int sifraZaposlenog;

        public Profesor(string i, string p, DateTime dr, string j, string k, int c, string ki, string l, Image s, int sz, bool u) : base(i, p, dr, j, k, c, ki, l, s, u)
        {
            sifraZaposlenog = sz;
        }

        public override string DajIme()
        {
            return ime;
        }

        public override string DajPrezime()
        {
            return prezime;
        }

        public override DateTime DajDatumRodjenja()
        {
            return datumRodjenja;
        }

        public override string DajJMBG()
        {
            return jmbg;
        }

        public override int DajSifru()
        {
            return sifra;
        }

        public override string DajKomentar()
        {
            return komentar;
        }

        public int DajSifruZaposlenog()
        {
            return sifraZaposlenog;
        }

        public override int DajBrojZaduzenihKnjiga()
        {
            return brojZaduzenihKnjiga;
        }

        public override void PovecajZaduzenja()
        {
            brojZaduzenihKnjiga++;
        }

        public override void SmanjiZaduzenja()
        {
            brojZaduzenihKnjiga--;
        }

        public override string Clanstvo(int n)
        {
            if (n == 1)
            {
                return "Mjesecno";
            }
            else
            {
                return "Godisnje";
            }

        }

        public override double DajCijenu()
        {
            if (clanstvo == 1)
            {
                return cijena * (1 - popust);
            }
            else
                return cijena * (1 - popust) * 10;
        }

        public override string ToString()
        {
            return ime + " " + prezime + " " + sifra + " (profesor)" + "\n";
        }
    }
}
