﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Clan
{
    public class Uposlenik
    {
        static int brojac = 1;
        private int sifraUposlenog;
        private string ime;
        private string prezime;
        private string jmbg;
        private DateTime datumRodjenja;
        private string korisnickoIme;
        private string lozinka;
        private Image slika;
        int status;
        bool ulogovan;

        public string statusUString()
        {
            string vrati = " ";
            if (status == 1)
            {
                vrati = "(admin)";
            }
            else
            {
                vrati = "(uposlenik)";
            }

            return vrati;
        }

        public int SifraUposlenog
        {
            get
            {
                return sifraUposlenog;
            }

            set
            {
                sifraUposlenog = value;
            }
        }

        public string Ime
        {
            get
            {
                return ime;
            }

            set
            {
                ime = value;
            }
        }

        public string Prezime
        {
            get
            {
                return prezime;
            }

            set
            {
                prezime = value;
            }
        }

        public string Jmbg
        {
            get
            {
                return jmbg;
            }

            set
            {
                jmbg = value;
            }
        }

        public DateTime DatumRodjenja
        {
            get
            {
                return datumRodjenja;
            }

            set
            {
                datumRodjenja = value;
            }
        }

        public string KorisnickoIme
        {
            get
            {
                return korisnickoIme;
            }

            set
            {
                korisnickoIme = value;
            }
        }

        public string Lozinka
        {
            get
            {
                return lozinka;
            }

            set
            {
                lozinka = value;
            }
        }

        public Image Slika
        {
            get
            {
                return slika;
            }

            set
            {
                slika = value;
            }
        }

        public int Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public bool Ulogovan
        {
            get
            {
                return ulogovan;
            }

            set
            {
                ulogovan = value;
            }
        }

        public Uposlenik(string i, string p, DateTime dr, string j, string ki, string l, Image s, int st, bool u)
        {
            string danRodjenja = dr.Day.ToString();
            if (dr.Day < 10)
                danRodjenja = "0" + danRodjenja;

            string mjesecRodjenja = dr.Month.ToString();
            if (dr.Month < 10)
                mjesecRodjenja = "0" + mjesecRodjenja;

            string godinaRodjenja = dr.Year.ToString();

            if (!(j[0] == danRodjenja[0] && j[1] == danRodjenja[1] && j[2] == mjesecRodjenja[0] && j[3] == mjesecRodjenja[1] && j[4] == godinaRodjenja[1] && j[5] == godinaRodjenja[2] && j[6] == godinaRodjenja[3]))
                throw new ArgumentException("Pogresan JMBG.");

            ime = i;
            prezime = p;
            datumRodjenja = dr;
            jmbg = j;
            korisnickoIme = ki;
            lozinka = l;
            slika = s;
            status = st;
            ulogovan = u;
            sifraUposlenog = brojac++;
        }

        public override string ToString()
        {
            return ime + " " + prezime + " " + statusUString() + " " + sifraUposlenog + "\n";
        }
    }
}
