﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiga
{
    public abstract class Knjiga
    {
        public bool zaduzena { get; set; }
        abstract public int DajSifru();
        abstract public string DajNaslov();
        abstract public string[] DajSpisakAutora();
        abstract public string DajISBN();
        abstract public string DajZanr();
        abstract public int DajGodinuIzdanja();
        abstract public string DajNazivIzdavaca();
        abstract public override string ToString();
    }
}
