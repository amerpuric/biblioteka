﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiga
{
    public class NaucniRad : ObicnaKnjiga
    {
        public string imeKonferencije;
        public string oblastNauke;

        public NaucniRad(string n, string[] sa, string i, string z, string ni, int gi, string ik, string on) : base(n, sa, i, z, ni, gi)
        {
            imeKonferencije = ik;
            oblastNauke = on;
        }

        public override int DajSifru()
        {
            return sifra;
        }

        public override string DajNaslov()
        {
            return naslov;
        }

        public override string[] DajSpisakAutora()
        {
            return spisakAutora;
        }

        public override string DajISBN()
        {
            return isbn;
        }

        public override string DajZanr()
        {
            return zanr;
        }

        public override int DajGodinuIzdanja()
        {
            return godinaIzdanja;
        }

        public override string DajNazivIzdavaca()
        {
            return nazivIzdavaca;
        }

        public string DajImeKonferencije()
        {
            return imeKonferencije;
        }

        public string DajOblastNauke()
        {
            return oblastNauke;
        }

        public override string ToString()
        {
            return naslov + " " + zanr + " " + nazivIzdavaca + " " + oblastNauke + "\n";
        }

    }
}
