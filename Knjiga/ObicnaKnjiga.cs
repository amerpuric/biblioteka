﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Knjiga
{
    public class ObicnaKnjiga : Knjiga
    {
        static int broj = 1;
        public int sifra;
        public string naslov;
        public string[] spisakAutora;
        public string isbn;
        public string zanr;
        public string nazivIzdavaca;
        public int godinaIzdanja;
        public bool zaduzena;
        private const string ISBN_PATTERN = @"ISBN\x20(?=.{13}$)\d{1,5}([- ])\d{1,7}\1\d{1,6}\1(\d|X)$";

        public bool Zaduzena
        {
            get
            {
                return zaduzena;
            }

            set
            {
                zaduzena = value;
            }
        }

        public ObicnaKnjiga(string n, string[] sa, string i, string z, string ni, int gi)
        {
            Regex regex = new Regex(ISBN_PATTERN);
            
            if(!regex.IsMatch(i))
            {
                throw new ArgumentException("Pogresan ISBN.");
            }

            naslov = n;
            spisakAutora = sa;
            isbn = i;
            zanr = z;
            nazivIzdavaca = ni;
            godinaIzdanja = gi;
            sifra = broj++;
            zaduzena = false;
        }

        public override int DajSifru()
        {
            return sifra;
        }

        public override string DajNaslov()
        {
            return naslov;
        }

        public override string[] DajSpisakAutora()
        {
            return spisakAutora;
        }

        public override string DajISBN()
        {
            return isbn;
        }

        public override string DajZanr()
        {
            return zanr;
        }

        public override int DajGodinuIzdanja()
        {
            return godinaIzdanja;
        }

        public override string DajNazivIzdavaca()
        {
            return nazivIzdavaca;
        }

        public override string ToString()
        {
            return naslov + " " + " " + zanr + " " + godinaIzdanja + "\n";
        }
    }
}
