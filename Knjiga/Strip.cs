﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Knjiga
{
    public class Strip : ObicnaKnjiga
    {
        string[] spisakUmjetnika;
        string imeAnimatorskeKuce;
        int brojIzdanja;
        bool specijal;

        public Strip(string n, string[] sa, string i, string z, string ni, int gi, string[] su, string iak, int bi, bool s) : base(n, sa, i, z, ni, gi)
        {
            spisakUmjetnika = su;
            imeAnimatorskeKuce = iak;
            brojIzdanja = bi;
            specijal = s;
        }

        public override int DajSifru()
        {
            return sifra;
        }

        public override string DajNaslov()
        {
            return naslov;
        }

        public override string[] DajSpisakAutora()
        {
            return spisakAutora;
        }

        public override string DajISBN()
        {
            return isbn;
        }

        public override string DajZanr()
        {
            return zanr;
        }

        public override int DajGodinuIzdanja()
        {
            return godinaIzdanja;
        }

        public override string DajNazivIzdavaca()
        {
            return nazivIzdavaca;
        }

        public string[] DajSpisakUmjetnika()
        {
            return spisakUmjetnika;
        }

        public string DajImeAnimatorskeKuce()
        {
            return imeAnimatorskeKuce;
        }

        public int DajBrojIzdanja()
        {
            return brojIzdanja;
        } 

        public bool DaLiJeSpecijal()
        {
            return specijal;
        }

        public override string ToString()
        {
            return naslov + " " + zanr + " " + nazivIzdavaca + " " + imeAnimatorskeKuce + " " + brojIzdanja + "\n";
        }
    }
}
