﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjiga
{
    public class ZaduzenaKnjiga
    {
        private Knjiga knjiga;
        private DateTime datumZaduzivanja;
        private DateTime rokVracanja;
        private DateTime datumVracanja;

        public ZaduzenaKnjiga(Knjiga k, DateTime rv)
        {
            knjiga = k;
            datumZaduzivanja = DateTime.Now;
            
            if(DateTime.Compare(datumZaduzivanja, rv) >= 1)
            {
                throw new ArgumentException("Pogresan datum vracanja knjige.");
            }

            rokVracanja = rv;
        }

        public Knjiga Knjiga
        {
            get
            {
                return knjiga;
            }
        }

        public DateTime DatumZaduzivanja
        {
            get
            {
                return datumZaduzivanja;
            }
        }

        public DateTime RokVracanja
        {
            get
            {
                return rokVracanja;
            }
        }

        public bool DaLiJeVracena
        {
            get
            {
                return datumVracanja != null;
            }
        }

        public void RazduziKnjigu()
        {
            if(DaLiJeVracena)
            {
                throw new InvalidOperationException("Knjiga je razduzena.");
            }

            datumVracanja = DateTime.Now;
        }

        
    }
}
